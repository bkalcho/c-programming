/* Exercise 2-4. Write an alternate version of squeeze(s1, s2) that deletes
   each character in s1 that matches any character in the string s2.

squeeze(s1, s2): function that deletes each character in s1 that matches
any character in the string s2.
AUTHOR: Bojan G. Kalicanin
DATE: 10-Jul-2016
VERSION: $Id$
*/

void squeeze(char s1[], char s2[]) {
    int i, j, k;

    for (i = j = 0; s1[i] != '\0'; i++) { 
        for (k = 0; s2[k] != '\0' && s2[k] != s1[i]; k++);
        if (s2[k] == '\0')   /* end of string - no match */
                s1[j++] = s1[i];
    }
    s[j] = '\0';
}