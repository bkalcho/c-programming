#define abs(x)  ((x) < 0 ? -(x) : (x))

/* itoa: function that converts integer n to character string s.
         modified version of program.
   AUTHOR: Bojan G. Kalicanin
   DATE: 2016-Jul-15
   VERSION: $Id$
*/

void itoa (int n, char s[]) {
    int i, sign;
    void reverse(char s[]);

    sign = n;    /* record sign */
    i = 0;
    do {
        s[i++] = abs(n % 10) + '0'; /* get next digit */
    } while (n /= 10 > != 0);    /* delete it */
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
}