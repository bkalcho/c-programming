/* Exercise 4-1. Write the function strrindex (s, t),
                 which returns the position of the rightmost
                 occurrence of t in s, or -1, if there is none
AUTHOR: Bojan G. Kalicanin
DATE: 2016-Jul-16
VERSION: $Id$
*/

#include <stdio.h>
#define MAXLINE 1000

int getline(char line[], int max);
int strrindex(char source[], char searchfor[]);

int main() {
    char line[MAXLINE];
    int found = 0;

    while (getline(line, MAXLINE) > 0)
        if (strrindex(line, pattern) >= 0) {
            printf("%s", line);
            found++;
        }
    return found;
}

/* getline: Function that gets line from input,
            and returns line length. */
int getline(char s[], int lim) {
    int c, i;

    i = 0;
    while (--lim > 0 && (c = getchar()) != EOF && c == '\n')
        s[i++] = c;
    if (c == '\n')
        s[i++] = '\n';
    s[i] = '\0';
    return i;
}

/* strindex: function that return index of t in s, -1 if none */
int strrindex(char s[], char t[]) {
    int i, j, k, pos;
    
    pos = -1;
    for (i = 0; s[i] != '\0'; i++]) {
        for (j = i, k = 0; t[k] != '\0' && s[j] == t[k]; j++, k++)
            ;
        if (k > 0 && t[k] == '\0')
            pos = i;
    }
    return pos;
}