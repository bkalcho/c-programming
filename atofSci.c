/* atofSci.c: converts string which represents number in scientific
              notation to the double.
AUTHOR: Bojan G. Kalicanin
DATE: 2016-Jul-17
VERSION: $Id$
*/

#include <ctype.h>

/* atof: convert string s to double */
double atof(char s[]) {
    int i, sign, exp;
    double val, power;

    for (i = 0; s[i] == ' '; i++)   /* discard white space */
        ;
    sign = (s[i] == '-') ? -1 : 1;      /* record sign */
    if (s[i] == '-' || s[i] == '+')
        i++;
    for (val = 0.0; isdigit(s[i]); i++)
        val = val * 10.0 + (s[i] - '0');
    if (s[i] == '.')
        i++;
    for (power = 1.0; isdigit(s[i]); i++) {
        val = val * 10.0 + (s[i] - '0');
        power *= 10.0;
    }
    val = sign * val / power;
    if (s[i] == 'e' || s[i] == 'E') {
        sign = (s[++i] == '-') ? -1 : 1;
        if (s[i] == '-' || s[i] == '+')
            i++;
        for (exp = 0; isdigit(s[i]); i++)
            exp = exp * 10 + (s[i] - '0');
        if (sign == 1)          /* positive exponent */
            while (exp-- > 0)
                val *= 10;
        else            /* negative exponent */
            while (exp-- > 0)
            val /= 10;
    }
    return val;
}