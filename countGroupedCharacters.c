/* countGroupedCharacters.c: Program to count the occurrences of each digit,
   white space, and all other characters.
   AUTHOR: Bojan G. Kalicanin
   DATE: 2016-Jul-13
   VERSION: $Id$
*/

#include <stdio.h>

int main() {
    int c, i, n_digit[10], n_white, n_other;

    n_white = n_other = 0;
    for (i = 0; i < 10; i++)
        n_digit[i] = 0;
    while ((c = getchar() != EOF) {
        switch (c) {
            case '1': case '2': case '3':
            case '4': case '5': case '6':
            case '7': case '8': case '9':
            case '0': n_digit[c - '0']++;
                      break;
            case ' ': case '\t':
            case '\n': n_white++;
                       break;
            default: n_other++;
                     break;
        }
    }
    printf("digits = ");
    for (i = 0; i < 10; i++)
        printf("%d ", n_digit[i]);
    printf("\nWhitespaces = %d", n_white);
    printf("\nOther characters = %d\n", n_other);
    return 0;
}