/* strindexOpt.c: Function which checks from the right side
            for the rightmost occurence of string t, and
            records its position.
AUTHOR: Bojan G. Kalicanin
DATE: 2016-Jul-16
VERSION: $Id$
*/

#include <string.h>

int strrindex(char s[], char t[]) {
    int i, j, k;

    for (i = strlen(s) - strlen(t); i >= 0; i--) {
        for (j = i, k = 0; t[k] != '\0' && s[j] == t[k]; j++, k++)
            ;
        if (k > 0 && t[k] == '\0')
            return i;
    }
    return -1;
}