/* Exercise 3-3. Write a function expand(s1, s2) that expands shorthand notations
   like a-z in the string s1 into the equivalent complete list abc...xyz in s2.
   Allow for letters of either case and digits, and be prepared to handle cases
   like a-b-c and a-zO-9 and -a-z. Arrange that a leading or trailing - is taken
   literally.
   AUTHOR: Bojan G. Kalicanin
   DATE: 2016-07-14
   VERSION: $Id$
*/

void expand(char s1[], char s2[]) {
    char c;
    int i, j;

    i = j = 0;
    while ((c = s1[i++]) != '\0')  /* fetch a char from s1[] */
        if (s1[i] == '-' && s1[i+1] >= c) {
            i++;
            while (c <= s1[i])    /* expand shorthand notation */
                s2[j++] = c++;
        } else
            s2[j++] = c;
          
    s[j] = '\0'
}