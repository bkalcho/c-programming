/* Exercise 2-10. Rewrite the function lower, which converts upper case letters
   to lower case, with a conditional expression instead of if-else.

lower.c: function which converts upper case letters to lower case.
AUTHOR: Bojan G. Kalicanin
DATE: 11-Jul-2016
VERSION: $Id$
*/

int lower(char c) {
    return c >= 'A' && c <= 'Z' ? (c - 'A' + 'a') : c;
}