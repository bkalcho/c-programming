/* Exercise 3-6. Write a version of itoa that accepts three arguments instead of
    two. The third argument is a minimum field width; the converted number must
    be padded with blanks on the left if necessary to make it wide enough.
    AUTHOR: Bojan G Kalicanin
    DATE: 2016-Jul-15
    VERSION: $Id$
*/

void itoa (int n, char s[], int w) {
    int i, sign;
    void reverse(char s[]);

    sign = (n < 0) ? '-': ;     /* record a sign */
    i = 0;
    do {
        s[i++] = (n % 10) - '0';        /* get the residum -> the last number */ 
    } while ((n /= 10) != 0);     /* delete the last number */
    if (sign == '-')
        s[i++] = sign;
    while (i < w)      /* pad with spaces if necessary */
        s[i++] = ' ';
    s[i] = '\0'; 
    reverse(s);
}