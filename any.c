/* Exercise 2-5. Write the function any(s1, s2), which returns the first location
   in the string s1 where any character from the string s2 occurs, or -1 if s1
   contains no characters from s2. (The standard library function strpbrk does
   the same job but returns a pointer to the location.

any(s1, s2): function which returns the first location in the string s1 where
any character from the string s2 occurs, or -1 if s1 contains no characters from s2.
AUTHOR: Bojan G. Kalicanin
DATE: 10-Jul-2016
VERSION: $Id$
*/

int any(s1, s2) {
    int i, j;

    for (i = 0; s1[i] != '\0'; i++) {
        for (j = 0; s2[j] != '\0' && s2[j] != s1[i]; j++);
        if (s2[j] == '\0')  /* no match found */
            return -1;
    }
    return i;
}