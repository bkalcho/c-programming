/* Exercise 2-3. Write the function htoi(s), which converts a string of hexadecimal digits
   (including an optional 0x or 0X) into its equivalent integer value. The allowable digits
   are 0 through 9, a through f, and A through F.

htoi(s): function which converts a string of hexadecimal digits into its equivalent integer value.
AUTHOR: Bojan G. Kalicanin
DATE: 10-Jul-2016
VERSION: $Id$
*/

int htoi(char s[]) {
    int i, n;
    char t;

    n = 0;
    for (i = 0; s[i] != '\0' && s[i] >= '0' && s[i] <= '9' && s[i] >= 'a' && s[i] <= 'f'
        && s[i] >= 'A' && s[i] <= 'F'; ++i) {
        if (i == 0 && s[i] == '0')
            continue;
        else if (i == 1 && (s[i] == 'x' || s[i] == 'X'))
            continue;
        if (islower(s[i]))
            n = n * 16 + (s[i] - 'a' + 10);
        else if (isupper(s[i]))
            n = n * 16 + (tolower(s[i] - 'a' + 10);        
    }
    return n;
}