/* Exercise 3-5. Write the function itob(n, s, b) that converts
    the integer n into a base b character representation in the
    string s. In particular, itob(n, s, 16) formats n as a
    hexadecimal integer in s.
    AUTHOR: Bojan G. Kalicanin
    DATE: 2016-Jul-15
    VERSION: $Id$
*/

void itob(int n, char s[], int b) {
    char sign;
    int i, k;
    void reverse(char s[]);

    sign = (n < 0) ? '-' : ;    /* record a sign */
    i = 0;
    do {
        k = n % b;
        switch(k) {         /* determine residum */
            case 15: s[i++] = 'F';
                     break;
            case 14: s[i++] = 'E';
                     break;
            case 13: s[i++] = 'D';
                     break;
            case 12: s[i++] = 'C';
                     break;
            case 11: s[i++] = 'B';
                     break;
            case 10: s[i++] = 'A';
                     break;
            default: s[i++] = k - '0';
                     break;
        }    
    } while ((n /= b) != 0);     /* removes digit */
    if (sign == '-')
        s[i++] = sign;
    s[i] = '\0';
    reverse(s); /* reverse string */
}