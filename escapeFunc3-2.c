/* Exercise 3-2. Write a function escape(s, t) that converts characters like
   newline and tab into visible escape sequences like \n and \t as it copies the
   string t to s. Use a switch. Write a function for the other direction as well,
   converting escape sequences into the real characters.

   escapeFunc3-2.: file containing two functions for conversion of
   non-printing characters.
   AUTHOR: Bojan G. Kalicanin
   DATE: 2016-07-14
   VERSION: $Id$
*/

/* escape(s, t): function which copies string t to string s
                 and changes escape characters to there
                 escape sequences.
*/
void escape(char s[], char t[]) {
    int i, j;

    for (i = 0, j = 0; t[i] != '\0'; i++, j++)
        switch (t[i]) {
            case '\t': s[j] = '\\', s[++j] = 't';
                       break;
            case '\n': s[j] = '\\', s[++j] = 'n';
                       break;
            default: s[j] = t[i];
                     break;
        }
    t[++j] = '\0';
}

/* revertEscape(s, t): function which copies string t to string s
                       and changes sequence "\t" or "\n" to
                       escape sequence '\t' or '\n'.
*/
void revertEscape(char s[], char t[]) {
    int i, j;

    for (i = 0, j = 0; t[i] != '\0'; i++, j++)
        if (t[i] == '\\')
            switch (t[++i]) {
                case 't': s[j] == '\t';
                          break;
                case 'n': s[j] == '\n';
                          break;
                default: s[j] = '\\', s[++j] = t[i];
                         break;
            }
        else
            s[j] = t[i];
}