/* Exercise 2-7. Write a function invert(x, p, n) that returns x with the n bits
   that begin at position p inverted (i.e., 1 changed into 0 and vice versa), leaving
   the others unchanged.

invert(x, p, n): returns x with the n bits that begin at position p inverted.
AUTHOR: Bojan G. Kalicanin
DATE: 10-Jul-2016
VERSION: $Id$
*/

unsigned invert(unsigned x, int p, int n) {
    return x ^ (~(~0 << n) << (p + 1 - n));
}