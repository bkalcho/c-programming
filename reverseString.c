/* reverseCharacterString1-19.c: reverse input lines
AUTHOR: Bojan G. Kalicanin
DATE: 08-Jul-2016
GIT VERSION: $Id$
*/

#include <stdio.h>
#define MAX 100
#define ON 1
#define OFF 0

/* declaration of a function. */
void reverse(char s[]);

/* main program that reverses line from input */
int main() {
    int i, c, flag;
    char s[MAX];

    while (c = getchar() != EOF) {
        if (c = '\n' || c = ' ' || c = '\t') {
            flag = OFF;
        } else
            flag = ON;
        if (flag) {
            for (i = 0; c != '\n' && c != ' ' && c != '\t'; ++i) {
                s[i] = c;
                c = getchar();
            }
            s[i] = '\0'
            reverse(s);
        }
    }
}


/* reverse(s): function that reverses input string. */
void reverse(char s[]) {
    int i, j, n, c, ;

    i = 0;
    while ((c = getchar()) != '\0') {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    n = i;
    t[n] = '\0';
    for (i = 0, j = n - 1; s[i] != '\0'; ++i, --j)
        t[j] = s[i];
    for (i = 0; t[i] != '\0'; i++)
        printf("%s", t[i]);
}